%define         debug_package %{nil}

Name:           PilotGo-plugin-topology
Version:        1.0.3
Release:        1
Summary:        system application architecture detection plugin for PilotGo
License:        MulanPSL-2.0
URL:            https://gitee.com/openeuler/PilotGo-plugin-topology
Source0:        https://gitee.com/openeuler/PilotGo-plugin-topology/repository/archive/v%{version}.tar.gz
# tar -xvf Source0
# cd %{name}-v%{version}/web
# run 'yarn install and yarn build' in it
# tar -czvf %{name}-web.tar.gz dist
Source1:        PilotGo-plugin-topology-web.tar.gz

BuildRequires:  systemd
BuildRequires:  golang

%description
system application architecture detection plugin for PilotGo

%package        server
Summary:        PilotGo-plugin-topology server
Provides:       pilotgo-plugin-topology-server = %{version}-%{release}

%description    server
PilotGo-plugin-topology server.

%package        agent
Summary:        PilotGo-plugin-topology agent
Provides:       pilotgo-plugin-topology-agent = %{version}-%{release}

%description    agent
PilotGo-plugin-topology agent.

%prep
%setup -n %{name}-v%{version}
tar -xzvf %{SOURCE1}

%build
cp -rf dist/* server/handler/
# server
pushd server
GOWORK=off GO111MODULE=on go build -mod=vendor -tags=production -o PilotGo-plugin-topology-server main.go
popd
# agent
pushd agent
GOWORK=off GO111MODULE=on go build -mod=vendor -o PilotGo-plugin-topology-agent main.go
popd

%install
mkdir -p %{buildroot}/opt/PilotGo/plugin/topology/{server/log,agent/log}
# server
install -D -m 0755 server/PilotGo-plugin-topology-server %{buildroot}/opt/PilotGo/plugin/topology/server
install -D -m 0644 conf/topo_server.yaml.templete %{buildroot}/opt/PilotGo/plugin/topology/server/topo_server.yaml
install -D -m 0644 scripts/PilotGo-plugin-topology-server.service %{buildroot}%{_unitdir}/PilotGo-plugin-topology-server.service
# agent
install -D -m 0755 agent/PilotGo-plugin-topology-agent %{buildroot}/opt/PilotGo/plugin/topology/agent
install -D -m 0644 conf/topo_agent.yaml.templete %{buildroot}/opt/PilotGo/plugin/topology/agent/topo_agent.yaml
install -D -m 0644 scripts/PilotGo-plugin-topology-agent.service %{buildroot}%{_unitdir}/PilotGo-plugin-topology-agent.service

%post           server
%systemd_post PilotGo-plugin-topology-server.service

%preun          server
%systemd_preun PilotGo-plugin-topology-server.service

%postun         server
%systemd_postun PilotGo-plugin-topology-server.service
# Clear all installed files when uninstalling
rm -rf /opt/PilotGo/plugin/topology

%post           agent
%systemd_post PilotGo-plugin-topology-agent.service

%preun          agent
%systemd_preun PilotGo-plugin-topology-agent.service

%postun         agent
%systemd_postun PilotGo-plugin-topology-agent.service

%files          server
/opt/PilotGo/plugin/topology/server/PilotGo-plugin-topology-server
/opt/PilotGo/plugin/topology/server/topo_server.yaml
/opt/PilotGo/plugin/topology/server/log
%{_unitdir}/PilotGo-plugin-topology-server.service

%files          agent
/opt/PilotGo/plugin/topology/agent/PilotGo-plugin-topology-agent
/opt/PilotGo/plugin/topology/agent/topo_agent.yaml
/opt/PilotGo/plugin/topology/agent/log
%{_unitdir}/PilotGo-plugin-topology-agent.service

%changelog
* Tue Apr 02 2024 jianxinyu <jiangxinyu@kylinos.cn> - 1.0.3-1
- Update package to version 1.0.3

* Tue Mar 19 2024 wangjunqi <wangjunqi@kylinos.cn> - 1.0.2-1
- add custom topo feature

* Wed Oct 18 2023 wangjunqi <wangjunqi@kylinos.cn> - 1.0.1-2
- change configuration file path to /opt

* Tue Oct 10 2023 wangjunqi <wangjunqi@kylinos.cn> - 1.0.1-1
- Package init

